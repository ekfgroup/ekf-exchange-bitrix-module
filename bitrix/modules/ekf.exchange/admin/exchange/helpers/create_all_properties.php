<?
/**
 * Создает все свойства из АПИ и сохраняет маппинг (настройки модуля => свойства)
 * Проверяет свойство на существование по коду, если свойство найдено в бд сайта - оно не будет повтороно создано
 */

use Ekf\Exchange\Service\ApiClient;
use Ekf\Exchange\Service\Config;

require(__DIR__ . '/../init.php');

$configManager = new Config();

$apiClient = new ApiClient($configManager->getApiKey());

$iblockId = $configManager->getIblockId();

if ($iblockId == 0) {
    exit('iblock id not set');
}

$propertiesMap = $configManager->getPropertiesMap();

$apiProperties = $apiClient->getProperties();

// {{{
$propsIterator = \Bitrix\Iblock\PropertyTable::getList([
    'filter' => [
        //'=ID' => 1983,
        '=IBLOCK_ID' => $iblockId
    ]
]);

$existingPropCodes = [];

while ($property = $propsIterator->fetch()) {
    if (!empty($property['CODE'])) {
        $existingPropCodes[$property['CODE']] = true;
    }
}
// }}}


$propertyManager = new CIBlockProperty();

foreach ($apiProperties as $apiProperty) {
    if (array_key_exists($apiProperty['id'], $propertiesMap)) {
        continue;
    }

    $sitePropertyCode = CUtil::translit($apiProperty['name'], 'ru', [
        'replace_space' => '_',
        'replace_other' => '_',
        'change_case'   => 'U'
    ]);

    $sitePropertyCode = preg_replace('/^[0-9_]+/', '', $sitePropertyCode);

    if (array_key_exists($sitePropertyCode, $existingPropCodes)) {
        $propertiesMap[$apiProperty['id']] = $sitePropertyCode;
        continue;
    }

    $featureList = [
        // Показывать на странице списка элементов
        'iblock:LIST_PAGE_SHOW' => [
            'ID' => 'n0',
            'MODULE_ID' => 'iblock',
            'FEATURE_ID' => 'LIST_PAGE_SHOW',
            'IS_ENABLED' => 'Y'
        ],
        // Показывать на детальной странице элемента
        'iblock:DETAIL_PAGE_SHOW' => [
            'ID' => 'n0',
            'MODULE_ID' => 'iblock',
            'FEATURE_ID' => 'DETAIL_PAGE_SHOW',
            'IS_ENABLED' => 'Y'
        ],
    ];

    $propertyData = [
        'NAME' => $apiProperty['name'],
        'XML_ID' => $apiProperty['id'],
        'CODE' => $sitePropertyCode,
        'PROPERTY_TYPE' => 'S',
        'IBLOCK_ID' => $iblockId,
        'FEATURES' => $featureList
    ];

    $existingProperty = CIBlockProperty::GetList([], ['IBLOCK_ID' => $iblockId, 'XML_ID' => $propertyData['XML_ID']])->Fetch();
    if ($existingProperty) {
        $propertiesMap[$apiProperty['id']] = $existingProperty['CODE'];
        continue;
    }

    $propertyId = $propertyManager->Add($propertyData);
    if (!$propertyId) {
        echo 'Ошибка создания свойства: ' . $propertyManager->LAST_ERROR;
        echo '<pre>';print_r($propertyData);echo '</pre>';
        exit();
        continue;
    }

    echo '<pre>';print_r($propertyId);echo '</pre>';

    $propertiesMap[$apiProperty['id']] = $sitePropertyCode;
}

$configManager->savePropertiesMap($propertiesMap);

echo 'done';

?>