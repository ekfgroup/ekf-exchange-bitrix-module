<?
/**
 * Находит соответствие свойств сайта и АПИ по наименованию и сохраняем маппинг
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

require(__DIR__ . '/../init.php');

$apiProperties = $apiClient->getProperties();
$propertiesMap = $configManager->getPropertiesMap();

$siteProperties = [];

$iterator = CIBlockProperty::GetList([], ['IBLOCK_ID' => $configManager->getIblockId()]);
while ($property = $iterator->Fetch()) {
    if ($property['CODE']) {
        $siteProperties[] = [
            'NAME' => $property['NAME'],
            'CODE' => $property['CODE'],
        ];
    }
}

foreach ($apiProperties as $apiProperty) {
    if (array_key_exists($apiProperty['id'], $propertiesMap)) {
        continue;
    }

    $bestMatch = [];
    $minDistance = 10000;

    foreach ($siteProperties as $siteProperty) {
        $distance = levenshtein($apiProperty['name'], $siteProperty['NAME']);
        if ($distance < $minDistance) {
            $bestMatch = $siteProperty;
            $minDistance = $distance;
        }
    }

    $propertiesMap[$apiProperty['id']] = $bestMatch['CODE'];
}

$configManager->savePropertiesMap($propertiesMap);