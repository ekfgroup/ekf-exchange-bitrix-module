<?
/**
 * Прогружаем все картинки из АПИ
 */


use Ekf\Exchange\Service\ApiClient;
use Ekf\Exchange\Service\Config;
use Ekf\Exchange\Functions\Categories as CatFunctions;

require(__DIR__ . '/../init.php');

$configManager = new Config();

$apiClient = new ApiClient($configManager->getApiKey());

$iblockId = $configManager->getIblockId();

$apiSections = CatFunctions\getSortedApiSection($apiClient);

foreach ($apiSections as $apiSection) {

    $existingSection = CatFunctions\getSectionByXmlId($iblockId, $apiSection['id']);

    if ((int)$existingSection['ID'] > 0 && (int)$existingSection['PICTURE'] == 0) {

        $sectionUpdateFields = [
            '+IMG' => $apiSection['mainImage'],
        ];

        CatFunctions\updateSection($existingSection['ID'], $sectionUpdateFields);
    }
}

echo 'done';

