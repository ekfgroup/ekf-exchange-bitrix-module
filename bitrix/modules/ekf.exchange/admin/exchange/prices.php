<?php
/**
 * @global CMain $APPLICATION
 */

use Ekf\Exchange\Service\Config;
use Ekf\Exchange\Service\DataDownloader;

require(__DIR__ . '/init.php');

// Настройка скидок/наценок на цены из АПИ
$discountsMap = $configManager->getPriceDiscountsMap();

//DELETE FROM b_catalog_price WHERE PRODUCT_ID IN (SELECT ID FROM b_iblock_element WHERE IBLOCK_ID=......)

try {
    $logger = new \Ekf\Exchange\Service\Logger('prices');

    $logger->state('Начат обмен ценами');

    if (empty($configManager->getPricesMap())) {
        throw new Exception('В настройках не указано соответствие цен');
    }

    $queueRepository = new \Ekf\Exchange\Repository\QueueRepository();

    $pricesFinder = new \Ekf\Exchange\Service\ProductPriceFinder(
        $iblockId,
        $configManager->getProductIdentifyMethod(),
        $configManager->getPropertiesMap());

    $pricesMap = $configManager->getPricesMap();

    $totalRecords = (new DataDownloader)->loadDataToQueue(
        [$apiClient, 'getPrices'], false, 'price'
    );

    $identifyByVendorCode = $configManager->getProductIdentifyMethod() === Config::IDENTIFY_BY_VENDOR_CODE;

    do {
        $totalRecords = $queueRepository->getQueueSize();

        $records = $queueRepository->getFromQueue(50);

        foreach ($records as $record) {

            try {
                $product = [];

                $product = unserialize($record['data'], ['allowed_classes' => false]);

                if (!is_array($product)) {
                    throw new Exception(sprintf('Ошибка десериализации: %s', $record['data']));
                }

                echo $product['id'] . ' / ' . ($totalRecords--) . PHP_EOL;

                ob_end_flush();

                $existingProduct = $pricesFinder->getExisting($identifyByVendorCode ? $product['vendorCode'] : $product['id']);

                if (!$existingProduct) {
                    continue;
                }

                $pricesForUpdate = [];

                /**
                 * Определим, какие цены изменились
                 */
                foreach ($product['prices'] as $price) {
                    $apiPriceId = $price['price_type']['id'];
                    $sitePriceId = $pricesMap[$apiPriceId];

                    if (isset($sitePriceId)) {
                        $apiPrice = $price['price'];

                        // Получим цену с учетом скидок/наценок
                        if (isset($discountsMap[$apiPriceId])) {
                            $apiPrice = round($apiPrice * (1 + (int)$discountsMap[$apiPriceId] / 100));
                        }

                        if ($apiPrice != $existingProduct['PRICES'][$apiPriceId]['price']) {
                            $pricesForUpdate[$pricesMap[$price['price_type']['id']]] = [
                                'price_value_id' => $existingProduct['PRICES'][$apiPriceId]['id'],
                                'price'          => $apiPrice
                            ];
                        }
                    }
                }


                /**
                 * Обновим изменившиеся
                 */
                foreach ($pricesForUpdate as $priceTypeId => $priceForUpdate) {
                    $priceFields = [
                        'PRODUCT_ID'       => $existingProduct['ID'],
                        'CATALOG_GROUP_ID' => $priceTypeId,
                        'PRICE'            => $priceForUpdate['price'],
                        //TODO::вынести в конфиг
                        'CURRENCY'         => 'RUB'
                    ];

                    if ($priceForUpdate['price_value_id'] > 0) {
                        $result = CPrice::Update($priceForUpdate['price_value_id'], $priceFields);
                    } else {
                        $result = CPrice::Add($priceFields);
                    }

                    if (!$result) {
                        $error = '';
                        $exception = $APPLICATION->GetException();
                        if ($exception) {
                            $error = $exception->GetString();
                        }

                        throw new Exception(sprintf(
                            'При попытке обновления цены для товара %s произошла ошибка: %s',
                            $existingProduct['ID'],
                            $error
                        ));
                    }

                    $logger->info(sprintf(
                        'Для товара %s [%d] обновлена цена %d', $product['id'], $existingProduct['ID'], $priceTypeId
                    ));
                }

                $totalProductsProcessed++;

            } catch (Exception $ex) {
                $logger->critical($logger->formatException($ex), [
                    'product_id' => $product['id'],
                    'trace'      => $ex->getTraceAsString()
                ]);
            }
        }

        //TODO::remove
        //break;

    } while (count($records) > 0);


    echo 'TOTAL: ' . $totalProductsProcessed . PHP_EOL;

} catch (Exception $ex) {
    echo $ex->getMessage(); // На случай если сам логгер выбросил исключение

    $logger->critical($logger->formatException($ex), [
        'trace' => $ex->getTraceAsString()
    ]);
}

$logger->state('Завершен обмен ценами');