<?php
use Bitrix\Main\Application;

$scriptsDir = __DIR__ . '/exchange/';

if (isset($_GET['script'])) {
    $scriptFile = $scriptsDir . $_GET['script'];
    if (!file_exists($scriptFile)) {
        exit('Wrong script name');
    }

    require $scriptFile;

    die;
}

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin.php';

$APPLICATION->SetTitle('Обмен EKFGroup');

\Bitrix\Main\Loader::includeModule('ekf.exchange');

$app = Application::getInstance();

$configManager = new \Ekf\Exchange\Service\Config();

if (empty($configManager->getApiKey()) || $configManager->getIblockId() == 0) {
    echo 'Выполните настройку модуля, прежде чем запускать обмен';
    require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
    return;
}

$page = $APPLICATION->GetCurPage();

$tabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => "Запуск обмена",
        "TITLE" => "",
    )
);

$tabControl = new CAdminTabControl("tabControl", $tabs);

$tabControl->begin();
?>

<? $tabControl->beginNextTab(); ?>
    <tr>
        <td colspan="2">

            <p>Одновременно может работать только один обмен!</p>

            <a target="_blank" href="<?= $page ?>?script=full.php">Полный обмен</a>

            <br><br>

            <a target="_blank" href="<?= $page ?>?script=categories.php">Разделы</a>

            <br><br>

            <a target="_blank" href="<?= $page ?>?script=products.php&mode=products">Основная информация о товарах</a>

            <br><br>

            <a target="_blank" href="<?= $page ?>?script=products.php&mode=properties">Характеристики</a>

            <br><br>

            <a target="_blank" href="<?= $page ?>?script=files.php">Изображения</a>

            <br><br>

            <a target="_blank" href="<?= $page ?>?script=remains.php">Остатки</a>

            <br><br>

            <a target="_blank" href="<?= $page ?>?script=prices.php">Цены</a>

            <? if ($_GET['admin_mode'] == 'y') { ?>
            <br><br>

            <a target="_blank" href="<?= $page ?>?script=helpers/create_all_properties.php">Создать все свойства</a>

            <? } ?>
        </td>
    </tr>

<? $tabControl->end(); ?>

<? require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
