<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class ekf_exchange extends CModule
{
    public function __construct()
    {
        $arModuleVersion = array();

        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        
        $this->MODULE_ID = 'ekf.exchange';
        $this->MODULE_NAME = Loc::getMessage('BEX_D7DULL_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('BEX_D7DULL_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage('BEX_D7DULL_MODULE_PARTNER_NAME');
        $this->PARTNER_URI = 'https://ekfgroup.com';
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);

        $this->installDB();
        $this->installEvents();
        $this->installFiles();
    }

    public function doUninstall()
    {
        $this->uninstallEvents();
        $this->uninstallDB();
        $this->unInstallFiles();

        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function installDB()
    {
        if (Loader::includeModule($this->MODULE_ID))
        {
            // Таблица-очередь
            $sql = \Ekf\Exchange\Entity\Orm\QueueTable::getEntity()->compileDbTableStructureDump();

            $connection = \Bitrix\Main\Application::getConnection();

            $connection->queryExecute(sprintf('DROP TABLE IF EXISTS `%s`', \Ekf\Exchange\Entity\Orm\QueueTable::getTableName()));
            $connection->queryExecute($sql[0]);

            // Таблица с редиректами
            $sql = \Ekf\Exchange\Entity\Orm\RedirectTable::getEntity()->compileDbTableStructureDump();

            $connection = \Bitrix\Main\Application::getConnection();

            $connection->queryExecute(sprintf('DROP TABLE IF EXISTS `%s`', \Ekf\Exchange\Entity\Orm\RedirectTable::getTableName()));
            $connection->queryExecute($sql[0]);
            $connection->queryExecute(sprintf(
                'ALTER TABLE %s ADD INDEX (`old_url`(255))', \Ekf\Exchange\Entity\Orm\RedirectTable::getTableName()
            ));
        }
    }

    public function uninstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID))
        {
            // Установка происходит в options.php
            //(new \Ekf\Exchange\Service\Install\PropertyManager())->uninstallProperties();

            // $connection = \Bitrix\Main\Application::getConnection();
            // $connection->queryExecute(sprintf('DROP TABLE `%s`', \Ekf\Exchange\Entity\Orm\QueueTable::getTableName()));
        }
    }

    public function installEvents()
    {
        RegisterModuleDependences('iblock', 'OnAfterIblockElementUpdate', $this->MODULE_ID, \Ekf\Exchange\Handler\IblockElementHandler::class, 'resetHashesOnElementChange');
        RegisterModuleDependences('iblock', 'OnBeforeIblockSectionUpdate', $this->MODULE_ID, \Ekf\Exchange\Handler\IblockSectionHandler::class, 'resetHashesOnSectionChange');
    }

    public function uninstallEvents()
    {
        UnRegisterModuleDependences('iblock', 'OnAfterIblockElementUpdate', $this->MODULE_ID);
        UnRegisterModuleDependences('iblock', 'OnBeforeIblockSectionUpdate', $this->MODULE_ID);
    }

    function installFiles($arParams = array())
    {
        CopyDirFiles($this->getModuleDir() . '/install/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        
        return true;
    }

    function unInstallFiles()
    {
        DeleteDirFiles($this->getModuleDir() . '/install/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        
        return true;
    }

    private function getModuleDir()
    {
        $dir = realpath(__DIR__ . '/../');
        return $dir;
    }
}
