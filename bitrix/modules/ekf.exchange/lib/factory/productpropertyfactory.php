<?php
namespace Ekf\Exchange\Factory;

use Ekf\Exchange\Entity\ProductEntity;
use Ekf\Exchange\Exception\BadProductDataException;
use Ekf\Exchange\Exception\BadPropertyDataException;

/**
 * @see ProductFactory
 */
class ProductPropertyFactory
{
    /** @var PropertyValueFactory  */
    private $propertyValueFactory;

    public function __construct()
    {
        $this->propertyValueFactory = new PropertyValueFactory();
    }

    public function buildFromArray($productData)
    {
        if (!is_array($productData) || empty($productData)) {
            throw new \InvalidArgumentException('Данные о товаре должны быть непустым массивом');
        }

        $productEntity = new ProductEntity();

        $productEntity->setXmlId(
            $this->getField($productData, 'id')
        );

        $productEntity->setName(
            $this->getField($productData, 'shortName')
        );

        $productEntity->setVendorCode($this->getField($productData, 'vendorCode'));

        foreach ($productData['properties'] as $property) {
            try {
                $propValue = !empty($property['etim_value']) ? $property['etim_value'] : $property['etim_value'];
                $propertyEntity = $this->propertyValueFactory->buildFromArray([
                    'api_code' => $property['id'],
                    'value' => trim($propValue),
                    'name' => $property['name'],
                    'unit' => $property['unit'],
                    'etim_unit' => $property['etimUnit'],
                ]);

                $productEntity->addToPropertyCollection($propertyEntity);
            } catch (BadPropertyDataException $ex) {

            }
        }

        return $productEntity;
    }

    /**
     * @param array $productData
     * @param $code
     * @param bool $required
     * @return string
     * @throws BadProductDataException
     */
    private function getField(array $productData, $code, $required = true)
    {
        $value = trim($productData[$code]);

        if ($required && empty($value)) {
            throw new BadProductDataException(sprintf(
                'В данных товара %s отсутствует либо пусто обязательное поле %s', $productData['id'], $code
            ));
        }

        return $value;
    }
}