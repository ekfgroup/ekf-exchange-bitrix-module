<?php
/**
 * @author Pavel Ivanov <itmariacchi@gmail.com>
 */

namespace Ekf\Exchange\Service;

class Output
{
    /**
     * @var string
     */
    private $lineEnding;

    public function __construct()
    {
        $this->lineEnding = PHP_SAPI === 'cli' ? PHP_EOL : '<br>';
    }

    /**
     * @param string $message
     */
    public function writeLn(string $message)
    {
        echo $message . $this->lineEnding;
    }
}