<?php
namespace Ekf\Exchange\Service;

use Bitrix\Catalog\Model\Product;
use Bitrix\Catalog\StoreProductTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Web\Json;
use CCatalogProduct;

class RemainsUpdater
{
    private $warehousesMap = [];

    /**
     * @var CCatalogProduct
     */
    private $catalogProductRepository;

    public function __construct()
    {
        Loader::includeModule('catalog');

        $iterator = \CCatalogStore::GetList([], ['!XML_ID' => false]);

        while ($warehouse = $iterator->Fetch()) {
            $this->warehousesMap[$warehouse['XML_ID']] = $warehouse['ID'];
        }

        $this->catalogProductRepository = new CCatalogProduct();
    }

    /**
     * Обновляет суммарное количество товара на складе
     * @param $productId
     * @param $totalProductQuantity
     * @param $updateMode
     * @return int
     * @throws \Exception
     */
    public function updateTotalQuantity($productId, $totalProductQuantity, $updateMode)
    {
        $fields = [
            'ID' => $productId,
            'QUANTITY' => $totalProductQuantity
        ];


        if ($updateMode) {
            $result = $this->catalogProductRepository->Update($productId, $fields);
        } else {
            $result = $this->catalogProductRepository->Add($fields);
        }

        if (!$result) {
            throw new \Exception(sprintf(
                'При попытке обновить суммарное количество для товара %d произошла ошибка', $productId
            ));
        }

        return $productId;
    }

    /**
     * Обновляет количество товара на складе
     * @param $productId
     * @param $warehouseXmlId
     * @param $quantity
     * @param $recordId
     * @return bool
     * @throws \Exception
     */
    public function updateWarehouseQuantity($productId, $warehouseXmlId, $quantity, $recordId)
    {
        $recordId = (int)$recordId;

        $warehouseId = (int)$this->warehousesMap[$warehouseXmlId];

        if ($warehouseId == 0) {
            throw new \Exception(sprintf(
                'Не удалось найти склад %s', $warehouseXmlId
            ));
        }

        $fields = [
            'PRODUCT_ID' => (int)$productId,
            'STORE_ID' => $warehouseId,
            'AMOUNT' => $quantity
        ];

        if ($recordId > 0) {
            $result = \CCatalogStoreProduct::Update($recordId, $fields);
        } else {
            $result = \CCatalogStoreProduct::Add($fields);
        }

        if (!$result) {
            throw new \Exception(sprintf('При попытке обновить количество на складе для товара %d (запись %d) произошла ошибка. Поля:%s',
                $productId,
                $recordId,
                Json::encode($fields)
            ));
        }

        return true;
    }

    /**
     * Удаляет все остатки для товара (по складам и суммарные)
     * @param $productId
     * @throws \Exception
     */
    public function clearRemains($productId)
    {
        $result = StoreProductTable::delete($productId);

        if (!$result->isSuccess()) {
            throw new \Exception(sprintf(
                'При попытке обнулить остатки по складам для товара %d произошла ошибка: %s', $productId, implode(', ', $result->getErrorMessages())
            ));
        }

        $result = $this->catalogProductRepository->Update($productId, ['QUANTITY' => 0]);

        if (!$result) {
            throw new \Exception(sprintf(
                'При попытке обнулить суммарные остатки для товара %d произошла ошибка', $productId
            ));
        }
    }
}