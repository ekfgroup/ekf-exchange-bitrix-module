<?php
/**
 * Служит для помощи в сопоставлении разделов сайта и АПИ
 * Через настройки в админке задаем соответствие разделов АПИ и Сайта
 * При этом у каждого соответствия есть признак - схлопывать артикула
 * Это значит, что не нужно создавать подразделы АПИ, а все товары из подразделов выгружать в один раздел сайта
 * В противном случае будут созданы подразделы
 */
namespace Ekf\Exchange\Service;


class SectionMapHelper
{
    /** @var Config  */
    private $configManager;

    /** @var ApiClient  */
    private $apiClient;

    /** @var SectionFinder  */
    private $sectionsFinder;

    // ИД_АПИ => ИД_САЙТ
    private $sectionsReverseMap = [];

    private $apiSectionsPrepared = [];

    public function __construct()
    {
        $this->configManager = new Config();
        $this->apiClient = new ApiClient($this->configManager->getApiKey());
        $this->sectionsFinder = new SectionFinder($this->configManager->getIblockId());

        $this->init();
    }

    private function init()
    {
        // {{{
        // Получим карту разделов ИД_РАЗДЕЛА_САЙТА => [[ИД_РАЗДЕЛА_АПИ, СХЛОПЫВАТЬ]]
        // И сделаем реверсивную карту
        $sectionsMap = $this->configManager->getSectionsMap();

        $sectionsReverseMap = [];

        foreach ($sectionsMap as $siteId => $item) {
            foreach ($item as list($apiId, $collapse)) {
                $sectionsReverseMap[$apiId] = [
                    'id' => $siteId, 'collapse' => $collapse];
            }
        }

        $this->sectionsReverseMap = $sectionsReverseMap;
        // }}}

        // {{{
        $apiSectionsRaw = $this->apiClient->getCategories();

        // Отсортируем в порядке вложенности
        usort($apiSectionsRaw, function($a, $b){
            if ($a['hierarchyLevel'] == $b['hierarchyLevel']) {
                return 0;
            }
            return ($a['hierarchyLevel'] < $b['hierarchyLevel']) ? -1 : 1;
        });

        // Сделаем ИД раздела ключем массива
        $apiSections = [];
        foreach ($apiSectionsRaw as $apiSection) {
            $apiSections[$apiSection['id']] = $apiSection;
        }

        $this->apiSectionsPrepared = $apiSections;
        // }}}
    }

    /**
     * Обертка над getMatchedSectionRaw
     * Возвращает ид_сайта прямого родителя для выгрузки товара, у которого раздел апи равен $apiSectionId
     * @param $apiSectionId
     * @return int
     */
    public function getMatchedSectionForProduct($apiSectionId)
    {
        $mapSection = $this->getMatchedSection($apiSectionId);

        if (count($mapSection) == 0) {
            // На сайте есть раздел с таким XML_ID, выгружаем в него
            $sectionForExport = $this->sectionsFinder->getExisting($apiSectionId);

            if ($sectionForExport) {
                return $sectionForExport['ID'];
            }

            return 0;
        }

        if ($mapSection['direct'] || $mapSection['collapse']) {
            return (int)$mapSection['id'];
        }

        $section = $this->sectionsFinder->getExisting($apiSectionId);

        return (int)$section['ID'];
    }

    /**
     * Получает для раздела из апи соответствующий раздел сайта, куда его необходимо выгружать
     * При этом вернется не непосредственные раздел для выгрузки, а раздел-родитель. Например:
     * Раздел АПИ А4, Раздел сайта С1, Настроках соответствия в админке А1 - С1, Иерархия А1 - А2 - А3 - А4
     * Метод для раздела А4 вернет С1, т.к. именно он указан в настройках. Далее в коде мы должны будем по XML_ID найти непосредстенного родителя А3
     * Почему может быть ситуация, когда раздел не нужно выгружать:
     *  В настройках соответствий данный раздел АПИ не настроен для выгрузки
     *  У раздела-родителя стоит галочка "Схлопывать артикула"
     *
     * @param $apiSectionId
     * @return array
     */
    public function getMatchedSection($apiSectionId)
    {
        $firstLvlMap = $this->sectionsReverseMap[$apiSectionId];

        if ($firstLvlMap) {
            $firstLvlMap['direct'] = true;//признак того, что прямо для данного раздела есть соответствие (а не для родителя)
            return $firstLvlMap;
        }

        $parentId = $this->apiSectionsPrepared[$apiSectionId]['parentId'];

        $secondLvlMap = $this->sectionsReverseMap[$parentId];

        if ($secondLvlMap) {
            $secondLvlMap['direct_parent'] = true;// для прямого родителя есть соответствие
            return $secondLvlMap;
        }

        while($parentId) {
            $parentId = $this->apiSectionsPrepared[$parentId]['parentId'];

            $nextLvlMap = $this->sectionsReverseMap[$parentId];

            if ($nextLvlMap) {
                return $nextLvlMap;
            }

        }

        return [];
    }
}