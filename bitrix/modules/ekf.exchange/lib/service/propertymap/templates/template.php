<?
/**
 * @var array $apiProperties
 * @var array $propertiesMap
 */

CUtil::InitJSCore(['jquery']);
?>

<style><?= file_get_contents(__DIR__ . '/style.css') ?></style>

<script><?= file_get_contents(__DIR__ . '/script.js') ?></script>

<div class="property-mapper">

    <table>

        <tr>
            <td><b>Свойство из АПИ</b></td>
            <td><b>Свойство с сайта (код)</b></td>
        </tr>

        <?
        // Некоторые свойства в понимании Битрикс в АПИ вовсе не свойства, а часть данных товара
        // Поэтому запишем их вручную
        $extProps = [
            ['code' => 'nameFull', 'name' => 'Полное наименование'],
            ['code' => 'vendorCode', 'name' => 'Код производителя'],
            ['code' => 'status', 'name' => 'Статус'],
            ['code' => 'multiplicity', 'name' => 'Розничная кратность'],
            ['code' => 'bazoved', 'name' => 'Базовая единица'],
            ['code' => 'name', 'name' => 'Полное наименование'],
            ['code' => 'dopustr', 'name' => 'Дополнительные устройства'],
            ['code' => 'volume', 'name' => 'Объем'],
        ];
        ?>

        <? foreach ($extProps as $extProp) { ?>

            <tr>
                <td><?= $extProp['name'] ?></td>
                <td>
                    <input size="60" name="ekf_exchange[property_map][<?= $extProp['code'] ?>]" value="<?= $propertiesMap[$extProp['code']] ?>">
                </td>
            </tr>

        <? } ?>

        <? foreach ($apiProperties as $property) { ?>

            <tr>
                <td>
                    <?= $property['name'] ?>
                </td>
                <td>
                    <input size="60" name="ekf_exchange[property_map][<?= $property['id'] ?>]" value="<?= $propertiesMap[$property['id']] ?>">
                </td>
            </tr>

        <? } ?>

    </table>
</div>