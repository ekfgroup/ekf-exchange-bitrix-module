<?
/**
 * @var array $apiPrices
 * @var array $pricesMap
 * @var array $sitePriceTypes
 */

CUtil::InitJSCore(['jquery']);
?>

<style><?= file_get_contents(__DIR__ . '/style.css') ?></style>

<script><?= file_get_contents(__DIR__ . '/script.js') ?></script>

<div class="price-mapper">

    <p>В поле скидка/наценка можно указать число с минусом в начале для скидки</p>
    <p>Например -10 означает скидку 10%</p>
    <p>Оставьте поле пустым, если скидка/наценка не нужна</p>

    <table>

        <tr>
            <td><b>Цена из АПИ</b></td>
            <td><b>Цена с сайта</b></td>
            <td><b>Скидка/Наценка %</b></td>
        </tr>

        <? foreach ($apiPrices as $apiPrice) { ?>

            <tr>
                <td>
                    <?= $apiPrice['name'] ?>
                </td>
                <td>
                    <select name="ekf_exchange[price_map][<?= $apiPrice['id'] ?>]">

                        <option value="">---</option>

                        <? foreach ($sitePriceTypes as $sitePriceType) { ?>

                            <option
                                    <?= ($pricesMap[$apiPrice['id']] == $sitePriceType['ID']) ? 'selected' : '' ?>
                                    value="<?= $sitePriceType['ID'] ?>">

                                        <?= $sitePriceType['NAME'] ?> [<?= $sitePriceType['ID'] ?>]

                            </option>

                        <? } ?>

                    </select>
                </td>
                <td>
                    <input type="text" name="ekf_exchange[discount_map][<?= $apiPrice['id'] ?>]" value="<?= $disountsMap[$apiPrice['id']] ?>">
                </td>
            </tr>

        <? } ?>

    </table>
</div>