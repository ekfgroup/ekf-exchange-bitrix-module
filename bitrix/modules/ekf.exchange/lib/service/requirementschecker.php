<?php
/**
 * @author Pavel Ivanov <itmariacchi@gmail.com>
 */
namespace Ekf\Exchange\Service;

use Ekf\Exchange\Exception\ModuleRequirementsException;

class RequirementsChecker
{
    /**
     * @throws ModuleRequirementsException
     */
    public function checkModuleRequirements()
    {
        $version = phpversion();

        $errors = [];

        if ($version < '7.0') {
            $errors[] = 'Минимальная версия PHP, необходимая для работы модуля необходим - 7.0';
        }

        if (!function_exists('curl_init')) {
            $errors[] = 'Для работы необходимо расширение curl';
        }

        if (count($errors) > 0) {
            throw new ModuleRequirementsException(implode('; ', $errors));
        }
    }
}