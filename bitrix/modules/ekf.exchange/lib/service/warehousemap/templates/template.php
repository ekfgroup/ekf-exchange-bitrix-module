<?
/**
 * @var array $apiWarehouses
 * @var array $siteWarehouses
 * @var array $warehousesMap
 */

CUtil::InitJSCore(['jquery']);
?>

<style><?= file_get_contents(__DIR__ . '/style.css') ?></style>

<script><?= file_get_contents(__DIR__ . '/script.js') ?></script>

<div class="price-mapper">

    <p>У склада на сайте обязательно должно быть заполнено поле XML_ID, иначе он не будет выведен в настройках</p>

    <table>

        <tr>
            <td><b>Склад из АПИ</b></td>
            <td><b>Склад с сайта</b></td>
        </tr>

        <? foreach ($apiWarehouses as $apiWarehouse) { ?>

            <tr>
                <td>
                    <?= $apiWarehouse['name'] ?>
                </td>
                <td>
                    <select name="ekf_exchange[warehouses_map][<?= $apiWarehouse['id'] ?>]">

                        <option value="">Не выгружать</option>

                        <? foreach ($siteWarehouses as $siteWarehouse) { ?>

                            <option
                                    <?= ($warehousesMap[$apiWarehouse['id']] == $siteWarehouse['XML_ID']) ? 'selected' : '' ?>
                                    value="<?= $siteWarehouse['XML_ID'] ?>">

                                        <?= $siteWarehouse['TITLE'] ?>

                            </option>

                        <? } ?>

                    </select>
                </td>
            </tr>

        <? } ?>

    </table>
</div>