<?php
namespace Ekf\Exchange\Service;

use InvalidArgumentException;

class ProductPriceFinder
{
    private $productsCache = [];

    /** @var Config  */
    private $config;

    /**
     * @var string
     */
    private $matchMethod;

    /**
     * @var array
     */
    private $propsMap;

    public function __construct($iblockId, string $matchMethod, array $propsMap)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }

        $this->config = new Config();
        $this->matchMethod = $matchMethod;
        $this->propsMap = $propsMap;

        $this->fillPricesMap($iblockId);
    }

    /**
     * Заполняет массив данными о существующих ценах
     * @param $iblockId
     */
    private function fillPricesMap($iblockId)
    {
        $priceMap = $this->config->getPricesMap();

        $select = array('ID', 'XML_ID');

        $vendorCodePropCode = $this->propsMap['vendorCode'];

        if (!empty($vendorCodePropCode)) {
            $select[] = 'PROPERTY_' . $vendorCodePropCode;
        }

        foreach ($priceMap as $apiId => $sitePriceId) {
            $select[] = 'CATALOG_GROUP_' . $sitePriceId;
        }

        $identifyByVendorCode = $this->matchMethod === Config::IDENTIFY_BY_VENDOR_CODE && 'PROPERTY_' . $vendorCodePropCode;
        if ($identifyByVendorCode) {
            $mapKey = 'PROPERTY_' . $vendorCodePropCode . '_VALUE';
        } else {
            $mapKey = 'XML_ID';
        }

        $dbElements = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N', 'PROPERTY_EKF_IS_EKF' => 1),
            false,
            false,
            $select
        );

        $this->productsCache = [];

        while ($arElement = $dbElements->Fetch()) {
            $this->productsCache[$arElement[$mapKey]] = [];

            $prices = [];

            foreach ($priceMap as $apiId => $sitePriceId) {
                $prices[$apiId] = [
                    'id' => (int)$arElement['CATALOG_PRICE_ID_' . $sitePriceId],
                    'price' => $arElement['CATALOG_PRICE_' . $sitePriceId]
                ];
            }

            $this->productsCache[$arElement[$mapKey]] = [
                'ID' => $arElement['ID'],
                'PRICES' => $prices
            ];
        }
    }

    /**
     * @param string $key
     * @return array
     */
    public function getExisting(string $key): array
    {
        $existingProduct = $this->productsCache[$key];

        if (isset($existingProduct)) {
            return $existingProduct;
        }

        return [];
    }
}