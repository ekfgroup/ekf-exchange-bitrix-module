<?php
namespace Ekf\Exchange\Service;

class ProductFilesUpdater
{
    public function __construct()
    {

    }

    /**
     * Обновляет изображения товара
     *
     * @param int $productId ИД товара
     * @param string $propertyCode Код свойства, в котором хранятся изображения
     * @param array $apiImages Массив изображений, полученный через АПИ
     */
    public function update($productId, $iblockId, $propertyCode, $apiImages)
    {
        if ((int)$productId == 0) {
            throw new \InvalidArgumentException(sprintf(
                'Ид товара должен быть числом: "%s"', $productId
            ));
        }

        /**
         * Сделаем имя файла ключем массива
         */
        $orderedApiImages = [];

        foreach ($apiImages as $apiImage) {
            $orderedApiImages[$apiImage['+base_name']] = $apiImage;
        }

        $existingFiles = $this->getExistingFiles($productId, $iblockId, $propertyCode);

        /**
         * Пометим на удаление те изображения сайта, которых нет в АПИ
         */
        foreach ($existingFiles as &$image) {
            if (!in_array($image['EXTERNAL_ID'], $orderedApiImages)) {
                $image['del'] = 'Y';
            }
        }

        unset($image);

        /**
         * Добавим новые изображения из АПИ
         */
        foreach ($orderedApiImages as $apiImage) {
            $bFound = false;

            foreach ($existingFiles as $image) {
                if ($image['EXTERNAL_ID'] == $apiImage['+base_name']) {
                    $bFound = true;
                    break;
                }
            }

            if (!$bFound) {
                $fileData = \CFile::MakeFileArray($apiImage['file']);
                if (is_array($fileData) && count($fileData) > 0 && $fileData['type'] != 'unknown') {
                    $fileData['external_id'] = $apiImage['+base_name'];
                    $existingFiles[] = [
                        'VALUE' => $fileData,
                        'DESCRIPTION' => $apiImage['name']
                    ];
                }
            }
        }

        /**
         * Обновим данные в БД
         */
        \CIBlockElement::SetPropertyValuesEx($productId, $iblockId, [$propertyCode => $existingFiles]);

        foreach ($existingFiles as $file) {
            unlink($file['tmp_name']);
        }
    }

    /**
     * @param $productId
     * @param $iblockId
     * @param $propertyCode
     * @return array
     */
    private function getExistingFiles($productId, $iblockId, $propertyCode)
    {
        $filesIds = [];

        $dbElements = \CIBlockElement::GetList(
            array(),
            array('ID' => $productId, 'IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N'),
            false,
            false,
            array('ID', 'XML_ID', 'PROPERTY_' . $propertyCode)
        );

        while ($arElement = $dbElements->Fetch()) {
            $filesIds[] = $arElement['PROPERTY_' . $propertyCode .'_VALUE'];
        }

        $existingFiles = [];

        if (count($filesIds) > 0) {

            $filesIterator = \CFile::GetList([], ['@ID' => $filesIds]);

            while ($file = $filesIterator->Fetch()) {
                $existingFiles[] = $file;
            }
        }

        return $existingFiles;
    }
}