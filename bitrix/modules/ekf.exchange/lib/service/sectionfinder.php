<?php
namespace Ekf\Exchange\Service;

use InvalidArgumentException;

/**
 * Ищет разделы в инфоблоке, чтобы понять, существует ли такой раздел или нет
 */
class SectionFinder
{
    private $sectionsCache = [];

    public function __construct($iblockId)
    {
        $this->init($iblockId);
    }

    /**
     * @param $iblockId
     * @throws InvalidArgumentException
     */
    public function init($iblockId)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }

        $this->fillExistingSectionsMap($iblockId);
    }

    /**
     * Получает
     *
     * @param $sectionXmlId
     * @return array
     */
    public function getExisting($sectionXmlId)
    {
        $existingSection = $this->sectionsCache[$sectionXmlId];

        if (isset($existingSection)) {
            return $existingSection;
        }

        return [];
    }

    /**
     * Заполняет массив данными о существующих разделах
     * @param $iblockId
     */
    private function fillExistingSectionsMap($iblockId)
    {
        $dbElements = \CIBlockSection::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N'),
            false,
            array('ID', 'XML_ID')
        );

        $this->sectionsCache = [];

        while ($arElement = $dbElements->Fetch()) {
            $this->sectionsCache[$arElement['XML_ID']] = $arElement;
        }
    }
}