<?php
namespace Ekf\Exchange\Service;

use Ekf\Exchange\Entity\ProductEntity;

/**
 * Ответственен за то, чтобы определять, что именно изменилось в товаре
 */
class ChangesDetector
{
    /**
     * Получает хэш товара, чтобы можно было понять, изменилось ли что-то в товаре
     * @param ProductEntity $productEntity
     * @return string
     */
    public function getHash(ProductEntity $productEntity)
    {
        $productData = $productEntity->toState();

        return md5(serialize($productData));
    }

    /**
     * Сбрасывае признак того, что товар обновлен
     * Требуется, чтобы определить, какие товары после обмена не были обновлены и соответственно должны быть удалены
     * т.к. были удалены из АПИ
     * @param $iblockId
     */
    public function resetUpdatedFlag($iblockId)
    {
        $dbElements = \CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => $iblockId,
                'PROPERTY_EKF_IS_EKF' => 1,
                'PROPERTY_EKF_UPDATED' => 1,
                'CHECK_PERMISSIONS' => 'N'
            ),
            false,
            false,
            array('ID')
        );

        while ($arElement = $dbElements->Fetch()) {
            \CIBlockElement::SetPropertyValuesEx($arElement['ID'], $iblockId, ['EKF_UPDATED' => 0]);
        }
    }

    public function setUpdatedFlag($productId, $iblockId)
    {
        \CIBlockElement::SetPropertyValuesEx($productId, $iblockId, ['EKF_UPDATED' => 1]);
    }

    /**
     * В конце обмена все товары, у которых не установлен признак EKF_UPDATED отключаем
     * т.к. их нет в АПИ
     * @param int $iblockId
     * @return int
     */
    public function deactivateNonExistingProducts($iblockId)
    {
        $cntDeactivated = 0;

        $dbElements = \CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => $iblockId,
                'ACTIVE' => 'Y',
                'PROPERTY_EKF_IS_EKF' => 1,
                '!PROPERTY_EKF_UPDATED' => 1,
                'CHECK_PERMISSIONS' => 'N'
            ),
            false,
            false,
            array('ID')
        );

        $objElement = new \CIBlockElement();

        while ($arElement = $dbElements->Fetch()) {
            $objElement->Update($arElement['ID'], ['ACTIVE' => 'N']);
            $cntDeactivated++;
        }

        return $cntDeactivated;
    }
}