<?php
namespace Ekf\Exchange\Entity\Orm;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;

/**
 * Таблица для соответствия старых и новых урлов
 * Разделы и товары в одной таблице
 */

class RedirectTable  extends DataManager
{
    public static function getTableName()
    {
        return 'ekf_exchange_redirect';
    }

    public static function getMap()
    {
        return [
            new StringField('xml_id', [
                'primary' => true
            ]),
            new StringField('old_url'),
            new StringField('new_url'),
            new IntegerField('is_category') // 1 - раздел, иначе товар
        ];
    }
}