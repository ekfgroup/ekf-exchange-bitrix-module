<?php
namespace Ekf\Exchange\Entity;


class PropertyValueEntity
{
    private $propertyCode;

    /** @var  string код свойства из АПИ */
    private $apiCode;

    private $value;

    private $name;

    private $unit;

    private $etimUnit;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getApiCode()
    {
        return $this->apiCode;
    }

    /**
     * @param mixed $apiCode
     */
    public function setApiCode($apiCode)
    {
        $this->apiCode = $apiCode;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param mixed $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return mixed
     */
    public function getEtimUnit()
    {
        return $this->etimUnit;
    }

    /**
     * @param mixed $etimUnit
     */
    public function setEtimUnit($etimUnit)
    {
        $this->etimUnit = $etimUnit;
    }

    /**
     * @return mixed
     */
    public function getPropertyCode()
    {
        return $this->propertyCode;
    }

    /**
     * @param mixed $propertyCode
     */
    public function setPropertyCode($propertyCode)
    {
        $this->propertyCode = $propertyCode;
    }
}