<?php
/**
 * @author Pavel Ivanov <itmariacchi@gmail.com>
 * Date: 28.11.2018
 */
namespace Ekf\Exchange\Handler;

use Bitrix\Main\Loader;
use Ekf\Exchange\Service\Config;

class IblockSectionHandler
{
    /**
     * При изменении раздела сбросим хэши, чтобы при обмене данные раздела обновились
     * @param $arFields
     */
    public static function resetHashesOnSectionChange(&$arFields)
    {
        static $iblockId = -1;

        if ($iblockId == -1) {
            if (Loader::includeModule('ekf.exchange')) {
                $iblockId = (int)(new Config())->getIblockId();
            }
        }

        if ($arFields['skip_ekf_hash_reset'] === true) {
            return;
        }

        if ((int)$arFields['ID'] > 0 && $arFields['IBLOCK_ID'] == $iblockId) {
            $arFields['UF_EKF_HASH'] = '';
        }
    }
}