<?php
namespace Ekf\Exchange\Exception;

use Exception;

/**
 * Для раздела АПИ не найден соответствующий раздел сайта
 */
class NoMatchingSectionException extends Exception
{

}