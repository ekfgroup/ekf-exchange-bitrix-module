<?php
namespace Ekf\Exchange\Exception;

use Exception;

/**
 * Формат данных об одном товаре, полученных через АПИ, не соответствует ожидаемому
 * Например, не заполнено какое-либо поле
 */
class BadProductDataException extends Exception
{

}