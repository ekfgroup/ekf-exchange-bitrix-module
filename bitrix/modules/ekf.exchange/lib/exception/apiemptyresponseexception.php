<?php
namespace Ekf\Exchange\Exception;

/**
 * В ответе апи нет данных
 * Обычно это не ошибка. Просто означает, что запросили слишком большую страницу в пагинации и данных нет
 */
class ApiEmptyResponseException extends ApiException
{

}