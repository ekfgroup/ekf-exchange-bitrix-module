<?php
namespace Ekf\Exchange\Exception;

use Exception;

/**
 * Не соблюдены требования необходимые для работы модуля (например не установлено расширение ПХП)
 */
class ModuleRequirementsException extends Exception
{

}