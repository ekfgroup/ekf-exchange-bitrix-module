<?php
/**
 * @author Pavel Ivanov <itmariacchi@gmail.com>
 * Date: 28.11.2018
 */
namespace Ekf\Exchange\Repository;

class UserFieldRepository
{
    public function addUserField(array $data)
    {
        global $USER_FIELD_MANAGER;

        $entityFields = $USER_FIELD_MANAGER->GetUserFields($data['ENTITY_ID']);

        if (!array_key_exists($data['FIELD_NAME'], $entityFields)) {
            $oUserTypeEntity = new \CUserTypeEntity();

            $fieldId = $oUserTypeEntity->Add($data);

            if (!$fieldId) {
                throw new \Exception(sprintf('Не удалось создать поле %s', $data['ENTITY_ID']));
            }
        }
    }
}