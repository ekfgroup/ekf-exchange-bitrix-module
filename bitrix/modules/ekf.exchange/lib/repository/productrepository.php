<?php
namespace Ekf\Exchange\Repository;

use CIBlockElement;
use Ekf\Exchange\Entity\ProductEntity;
use Ekf\Exchange\Exception\ProductSaveException;
use InvalidArgumentException;

class ProductRepository
{
    /** @var CIBlockElement  */
    private $storage;

    public function __construct()
    {
        $this->storage = new CIBlockElement();
    }

    public function add(ProductEntity $productEntity)
    {
        $productData = $productEntity->toState();

        $productDataWithoutProperties = array_diff_key($productData, ['PROPERTIES' => 1]);

        // Если тут что-то работает медленно, см. обработчики событий
        // Если они добавляют что-то в $productDataWithoutProperties['PROPERTY_VALUES'] это негативно скажется на производительности
        // т.к. приведет к полному обновлению всех свойств
        $productId = $this->storage->Add($productDataWithoutProperties, false, false, false);

        if (!$productId) {
            throw new ProductSaveException(sprintf(
                'При добавлении товара %s произошла ошибка: %s', $productEntity->getXmlId(), $this->storage->LAST_ERROR
            ));
        }

        $properties = $productData['PROPERTIES'];

        if (count($properties) > 0) {
            CIBlockElement::SetPropertyValuesEx($productId, $productEntity->getIblockId(), $properties);
        }

        return $productId;
    }

    /**
     * Обновляет товар
     * Из за особенностей АПИ метод используется и для обновления основной информации о товаре и его свойств
     * За то, что именно будет обновлено отвечает флаг $bProdMode
     * @param $productId
     * @param $iblockId
     * @param ProductEntity $productEntity
     * @param $bProdMode - режим обновления товара
     * @return mixed
     * @throws ProductSaveException
     */
    public function update($productId, $iblockId, ProductEntity $productEntity, $bProdMode)
    {
        if ((int)$productId == 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид товара должен быть числом: "%s"', $productId
            ));
        }

        $productData = $productEntity->toState();

        // Нам не нужно перезаписывать все свойства, обновим их отдельно
        $properties = $productData['PROPERTIES'];
        unset($productData['PROPERTIES']);

        // Не нужно, чтобы срабатывал наш обработчик
        // bitrix/modules/ekf.exchange/lib/handler/iblockelementhandler.php
        $productData['skip_ekf_hash_reset'] = true;

        if ($bProdMode) {
            $bSuccess = $this->storage->Update($productId, $productData, false, false, false, false);

            if (!$bSuccess) {
                throw new ProductSaveException(sprintf(
                    'При обновлении товара %s [%d] произошла ошибка: %s', $productEntity->getXmlId(), $productId, $this->storage->LAST_ERROR
                ));
            }
        }

        if (!empty($properties)) {
            CIBlockElement::SetPropertyValuesEx($productId, $iblockId, $properties);
        }

        return $productId;
    }
}