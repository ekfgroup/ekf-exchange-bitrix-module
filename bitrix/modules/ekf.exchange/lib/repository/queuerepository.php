<?php
namespace Ekf\Exchange\Repository;

use Bitrix\Main\Application;
use Ekf\Exchange\Entity\Orm\QueueTable;
use Exception;
use InvalidArgumentException;

/**
 * Репозиторий
 */

class QueueRepository
{
    /**
     * Очищает очередь
     */
    public function clearQueue()
    {
        $connection = Application::getConnection();
        $connection->queryExecute(sprintf('TRUNCATE %s', QueueTable::getTableName()));
    }

    public function resetUpdatedFlag()
    {
        $connection = Application::getConnection();
        $connection->queryExecute(sprintf('UPDATE %s SET updated=0', QueueTable::getTableName()));
    }

    /**
     * @param $productId
     * @param $productData
     * @param $type
     * @throws Exception
     */
    public function enqueue($productId, $productData, $type)
    {
        if (empty($productId))
            throw new InvalidArgumentException('$productId не может быть пустым');

        if (empty($productData))
            throw new InvalidArgumentException('$productData не может быть пустым');

        if (empty($type))
            throw new InvalidArgumentException('$type не может быть пустым');

        $result = QueueTable::add([
            'product_id' => $productId,
            'data' => $productData,
            'type' => $type,
            'updated' => 0
        ]);

        if (!$result) {
            throw new Exception(sprintf(
                'При сохранении записи в очередь произошла ошибка: %s',
                implode(', ', $result->getErrorMessages())
            ));
        }
    }

    public function getFromQueue($numRecords = 0)
    {
        $connection = Application::getConnection();

        $params = [
            'filter' => [
                'updated' => 0,
            ]
        ];

        if ((int)$numRecords > 0) {
            $params['limit'] = $numRecords;
        }

        $records = QueueTable::getList($params)->fetchAll();

        if (count($records) > 0) {
            $connection->queryExecute(sprintf(
                'UPDATE %s SET updated=1 WHERE id IN (%s)', QueueTable::getTableName(), implode(', ', array_column($records, 'id'))
            ));
        }

        return $records;
    }

    /**
     * Метод нужен для получения остатков
     * Каждому товару соответствует несколько записей по остаткам, мы должны получить все разом
     *
     * @param $numProducts
     * @return array
     * @throws Exception
     */
    public function getFromQueueByProduct($numProducts)
    {
        $connection = Application::getConnection();

        $connection->startTransaction();

        try {
            $iterator = $connection->query(sprintf(
                    'SELECT DISTINCT product_id FROM `%s` WHERE updated=0 LIMIT %d', QueueTable::getTableName(), (int)$numProducts)
            );

            while ($product = $iterator->fetch()) {
                $productsToProcess[] = $product['product_id'];
            }

            $recordsRaw = $connection->query(sprintf(
                'SELECT * FROM `%s` WHERE product_id IN (\'%s\') FOR UPDATE',
                QueueTable::getTableName(),
                implode("', '", $productsToProcess)
            ))->fetchAll();

            if (count($recordsRaw) > 0) {
                $connection->queryExecute(sprintf(
                    'UPDATE %s SET updated=1 WHERE id IN (%s)', QueueTable::getTableName(), implode(', ', array_column($recordsRaw, 'id'))
                ));
            }

            $connection->commitTransaction();
        } catch (\Exception $ex) {
            $connection->rollbackTransaction();
            throw $ex;
        }

        $records = [];

        foreach ($recordsRaw as $item) {
            $records[$item['product_id']][] = $item;
        }

        return $records;
    }

    /**
     *
     */
    public function getQueueSize()
    {
        $connection = Application::getConnection();

        $result = $connection->query(sprintf(
            'SELECT COUNT(id) as queue_size FROM `%s` WHERE updated=0', QueueTable::getTableName())
        )->fetch();

        return (int)$result['queue_size'];
    }
}